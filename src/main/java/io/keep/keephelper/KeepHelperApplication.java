package io.keep.keephelper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;

@Slf4j
@EnableScheduling
@SpringBootApplication
@RequiredArgsConstructor
public class KeepHelperApplication implements ApplicationRunner {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(KeepHelperApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("start");
    }
}
