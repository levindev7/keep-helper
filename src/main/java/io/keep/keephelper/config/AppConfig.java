package io.keep.keephelper.config;

import io.keep.keephelper.controller.MessageFactory;
import io.keep.keephelper.controller.callbackTypes.AddressListByCallback;
import io.keep.keephelper.controller.callbackTypes.ConfigGeneratorByCallback;
import io.keep.keephelper.controller.callbackTypes.DockerByCallback;
import io.keep.keephelper.controller.callbackTypes.FaucetsByCallback;
import io.keep.keephelper.controller.callbackTypes.GitHubByCallback;
import io.keep.keephelper.controller.callbackTypes.GuidesByCallback;
import io.keep.keephelper.controller.callbackTypes.HelpByCallback;
import io.keep.keephelper.controller.callbackTypes.KeystoreByCallback;
import io.keep.keephelper.controller.callbackTypes.MainnetECSDAByCallback;
import io.keep.keephelper.controller.callbackTypes.MainnetRopstenByCallback;
import io.keep.keephelper.controller.callbackTypes.MoveToMainByCallback;
import io.keep.keephelper.controller.callbackTypes.TestnetECSDAByCallback;
import io.keep.keephelper.controller.callbackTypes.TestnetRopstenByCallback;
import io.keep.keephelper.controller.commandTypes.Help;
import io.keep.keephelper.controller.commandTypes.Start;
import io.keep.keephelper.service.impl.parsers.DockerParserImpl;
import io.keep.keephelper.service.interfaces.handlers.Callback;
import io.keep.keephelper.service.interfaces.handlers.Command;
import io.keep.keephelper.service.interfaces.parsers.AddressListParser;
import io.keep.keephelper.service.interfaces.parsers.ConfigParser;
import io.keep.keephelper.service.interfaces.parsers.FaucetsParser;
import io.keep.keephelper.service.interfaces.parsers.GitHubParser;
import io.keep.keephelper.service.interfaces.parsers.GuidesParser;
import io.keep.keephelper.service.interfaces.parsers.KeystoreParser;
import io.keep.keephelper.service.interfaces.parsers.MainnetECDSAParser;
import io.keep.keephelper.service.interfaces.parsers.MainnetRopstenParser;
import io.keep.keephelper.service.interfaces.parsers.TestnetECDSAParser;
import io.keep.keephelper.service.interfaces.parsers.TestnetRopstenParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static io.keep.keephelper.utils.Constants.ADDRESS_LIST_CALLBACK;
import static io.keep.keephelper.utils.Constants.BACK_TO_MAIN_CALLBACK;
import static io.keep.keephelper.utils.Constants.CONFIG_GENERATOR_CALLBACK;
import static io.keep.keephelper.utils.Constants.DOCKER_COMMANDS_CALLBACK;
import static io.keep.keephelper.utils.Constants.FAUCETS_CALLBACK;
import static io.keep.keephelper.utils.Constants.GITHUB_CALLBACK;
import static io.keep.keephelper.utils.Constants.GUIDES_CALLBACK;
import static io.keep.keephelper.utils.Constants.HELP_CALLBACK;
import static io.keep.keephelper.utils.Constants.HELP_COMMAND;
import static io.keep.keephelper.utils.Constants.KEYSTORE_CALLBACK;
import static io.keep.keephelper.utils.Constants.MAINNET_ECDSA_CALLBACK;
import static io.keep.keephelper.utils.Constants.MAINNET_ROPSTEN_RANDOM_CALLBACK;
import static io.keep.keephelper.utils.Constants.START_COMMAND;
import static io.keep.keephelper.utils.Constants.TESTNET_ECDSA_CALLBACK;
import static io.keep.keephelper.utils.Constants.TESTNET_ROPSTEN_RANDOM_CALLBACK;

@Configuration
public class AppConfig {

    @Bean
    public MessageFactory messageFactory(TestnetRopstenParser testnetRopstenParser, DockerParserImpl dockerParser,
                                         FaucetsParser faucetsParser, KeystoreParser keystoreParser, ConfigParser configParser,
                                         AddressListParser addressListByCallback, GitHubParser gitHubParser, GuidesParser guidesParser,
                                         TestnetECDSAParser testnetECDSAParser, MainnetRopstenParser mainnetRopstenParser,
                                         MainnetECDSAParser mainnetECDSAParser) {
        final MessageFactory factory = new MessageFactory();
        Map<String, Callback> callbackMap = new HashMap<>();
        Map<String, Command> commandMap = new HashMap<>();

        callbackMap.put(TESTNET_ROPSTEN_RANDOM_CALLBACK, new TestnetRopstenByCallback(testnetRopstenParser));
        callbackMap.put(MAINNET_ECDSA_CALLBACK, new MainnetECSDAByCallback(mainnetECDSAParser));
        callbackMap.put(MAINNET_ROPSTEN_RANDOM_CALLBACK, new MainnetRopstenByCallback(mainnetRopstenParser));
        callbackMap.put(TESTNET_ECDSA_CALLBACK, new TestnetECSDAByCallback(testnetECDSAParser));
        callbackMap.put(BACK_TO_MAIN_CALLBACK, new MoveToMainByCallback());
        callbackMap.put(CONFIG_GENERATOR_CALLBACK, new ConfigGeneratorByCallback(configParser));
        callbackMap.put(DOCKER_COMMANDS_CALLBACK, new DockerByCallback(dockerParser));
        callbackMap.put(FAUCETS_CALLBACK, new FaucetsByCallback(faucetsParser));
        callbackMap.put(KEYSTORE_CALLBACK, new KeystoreByCallback(keystoreParser));
        callbackMap.put(ADDRESS_LIST_CALLBACK, new AddressListByCallback(addressListByCallback));
        callbackMap.put(GITHUB_CALLBACK, new GitHubByCallback(gitHubParser));
        callbackMap.put(GUIDES_CALLBACK, new GuidesByCallback(guidesParser));
        callbackMap.put(HELP_CALLBACK, new HelpByCallback());

        commandMap.put(START_COMMAND, new Start());
        commandMap.put(HELP_COMMAND, new Help());

        factory.setCallbackMap(callbackMap);
        factory.setCommandMap(commandMap);

        return factory;
    }
}
