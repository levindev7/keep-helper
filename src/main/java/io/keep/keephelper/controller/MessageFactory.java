package io.keep.keephelper.controller;

import io.keep.keephelper.service.interfaces.handlers.Callback;
import io.keep.keephelper.service.interfaces.handlers.Command;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Getter
@Setter
public class MessageFactory {

    Map<String, Callback> callbackMap = new HashMap<>();
    Map<String, Command> commandMap = new HashMap<>();

    public Optional<Callback> getCallbackOperation(String operator) {
        return Optional.ofNullable(callbackMap.get(operator));
    }

    public Optional<Command> getCommandOperation(String operator) {
        return Optional.ofNullable(commandMap.get(operator));
    }
}