package io.keep.keephelper.controller.commandTypes;

import io.keep.keephelper.service.interfaces.handlers.Command;
import io.keep.keephelper.utils.MessageHelper;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import static io.keep.keephelper.utils.Constants.HELP_MESSAGE;

public class Help implements Command {

    @Override
    public SendMessage getCommand(long chatId) {

        return MessageHelper.sendMessage(chatId, HELP_MESSAGE);
    }
}
