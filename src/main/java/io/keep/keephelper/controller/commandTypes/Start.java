package io.keep.keephelper.controller.commandTypes;

import io.keep.keephelper.service.interfaces.handlers.Command;
import io.keep.keephelper.utils.MessageHelper;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import static io.keep.keephelper.utils.Constants.START_MESSAGE;

public class Start implements Command {

    @Override
    public SendMessage getCommand(long chatId) {

        return MessageHelper.sendMessage(chatId, START_MESSAGE);
    }
}
