package io.keep.keephelper.controller.callbackTypes;

import io.keep.keephelper.service.BotService;
import io.keep.keephelper.service.interfaces.handlers.Callback;
import io.keep.keephelper.service.interfaces.parsers.TestnetRopstenParser;
import io.keep.keephelper.utils.MessageHelper;
import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

@RequiredArgsConstructor
public class TestnetRopstenByCallback implements Callback {
    private final TestnetRopstenParser testnetRopstenParser;

    @Override
    public EditMessageText getCallback(long chatId, int messageId) {
        return MessageHelper.editMessageWithKeyboard(chatId,
                messageId, testnetRopstenParser.parse(BotService.users.get(chatId)), MessageHelper.setMainAndMoreButtons());
    }
}
