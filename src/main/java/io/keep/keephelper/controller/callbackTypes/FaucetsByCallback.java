package io.keep.keephelper.controller.callbackTypes;

import io.keep.keephelper.service.BotService;
import io.keep.keephelper.service.interfaces.handlers.Callback;
import io.keep.keephelper.service.interfaces.parsers.FaucetsParser;
import io.keep.keephelper.utils.MessageHelper;
import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

@RequiredArgsConstructor
public class FaucetsByCallback implements Callback {
    private final FaucetsParser faucetsParser;

    @Override
    public EditMessageText getCallback(long chatId, int messageId) {
        return MessageHelper.editMessageWithKeyboard(chatId,
                messageId, faucetsParser.parse(BotService.users.get(chatId)), MessageHelper.setMainAndMoreButtons());
    }
}
