package io.keep.keephelper.utils;

import com.vdurmont.emoji.EmojiParser;

public class Constants {
    public static final String GREEN_EMOJI = EmojiParser.parseToUnicode(":white_check_mark:");
    public static final String RED_EMOJI = EmojiParser.parseToUnicode(":no_entry_sign:");
    public static final String ORANGE_EMOJI = EmojiParser.parseToUnicode(":warning:");
    public static final String GRAY_EMOJI = EmojiParser.parseToUnicode(":arrows_counterclockwise:");

    public static final String FORWARD = "\u2192";
    public static final String BACKWARD = "\u2190";

    public static final String START_MESSAGE = "Welcome to Keep-Helper\n" +
            "Help, tools and resources for contributing to the Keep Network";
    public static final String HELP_MESSAGE = "If you need help, please contact us - https://t.me/crypto_Le\n" +
            "List of commands available to the bot:\n" +
            "/start - start\n" +
            "/help - help\n";
    public static final String INFURA_PROJECT_ID = "Enter Infura Project ID";
    public static final String TEMPLATE_TYPE = "Fill out a form to generate configuration files for your Keep nodes\n" +
            "Choose Template Type";
    public static final String DOCKER_COMMANDS = "Cheatsheet for commands to create, start, restart, and inspect your Keep nodes\n";

    public static final String START_COMMAND = "/start";
    public static final String HELP_COMMAND = "/help";

    public static final String TESTNET_ROPSTEN_RANDOM_CALLBACK = "testnet_ropsten";
    public static final String TESTNET_ECDSA_CALLBACK = "testnet_ecdsa";
    public static final String MAINNET_ROPSTEN_RANDOM_CALLBACK = "mainnet_ropsten";
    public static final String MAINNET_ECDSA_CALLBACK = "mainnet_ecdsa";
    public static final String CONFIG_GENERATOR_CALLBACK = "config";
    public static final String DOCKER_COMMANDS_CALLBACK = "docker";
    public static final String FAUCETS_CALLBACK = "faucets";
    public static final String KEYSTORE_CALLBACK = "keystore";
    public static final String ADDRESS_LIST_CALLBACK = "address";
    public static final String GITHUB_CALLBACK = "github";
    public static final String GUIDES_CALLBACK = "Guides";
    public static final String BACK_TO_MAIN_CALLBACK = "main";
    public static final String HELP_CALLBACK = "help";

}
