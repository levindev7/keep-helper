package io.keep.keephelper.utils;


import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.keep.keephelper.utils.Constants.ADDRESS_LIST_CALLBACK;
import static io.keep.keephelper.utils.Constants.BACKWARD;
import static io.keep.keephelper.utils.Constants.BACK_TO_MAIN_CALLBACK;
import static io.keep.keephelper.utils.Constants.CONFIG_GENERATOR_CALLBACK;
import static io.keep.keephelper.utils.Constants.DOCKER_COMMANDS_CALLBACK;
import static io.keep.keephelper.utils.Constants.FAUCETS_CALLBACK;
import static io.keep.keephelper.utils.Constants.GITHUB_CALLBACK;
import static io.keep.keephelper.utils.Constants.GUIDES_CALLBACK;
import static io.keep.keephelper.utils.Constants.HELP_CALLBACK;
import static io.keep.keephelper.utils.Constants.MAINNET_ECDSA_CALLBACK;
import static io.keep.keephelper.utils.Constants.MAINNET_ROPSTEN_RANDOM_CALLBACK;
import static io.keep.keephelper.utils.Constants.TESTNET_ECDSA_CALLBACK;
import static io.keep.keephelper.utils.Constants.TESTNET_ROPSTEN_RANDOM_CALLBACK;
import static io.keep.keephelper.utils.Constants.FORWARD;

@Slf4j
public class MessageHelper {

    public static SendMessage sendMessage(long chatId, String describe) {
        try {
            return new SendMessage()
                    .enableMarkdown(true)
                    .enableHtml(true)
                    .setChatId(chatId)
                    .setText(describe);
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E010] Error on execute bot command: {}, {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    public static SendMessage sendConfigMessage(long chatId, String describe) {
        try {
            return new SendMessage()
                    .enableMarkdown(true)
                    .enableHtml(true)
                    .setChatId(chatId)
                    .setText(describe);
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E011] Error on execute bot command: {}, {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    public static EditMessageText editMessage(long chatId, int messageId, String describe) {
        try {
            return new EditMessageText()
                    .enableMarkdown(true)
                    .setMessageId(messageId)
                    .setChatId(chatId)
                    .setText(describe);
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E012] Error on execute bot command: {}, {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    public static SendMessage sendMessageWithKeyboard(long chatId, String describe) {
        try {
            return new SendMessage()
                    .enableMarkdown(true)
                    .setChatId(chatId)
                    .setText(describe)
                    .setReplyMarkup(setMainAndMoreButtons());
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E013] Error on execute bot command: {}, {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    public static EditMessageText editMessageWithKeyboard(long chatId, int messageId, String text, InlineKeyboardMarkup keyboard) {
        try {
            return new EditMessageText()
                    .enableMarkdown(true)
                    .setMessageId(messageId)
                    .setChatId(chatId)
                    .setText(text)
                    .setReplyMarkup(keyboard);
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E014] Error on execute bot command: {}, {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    public static InlineKeyboardMarkup setMainAndMoreButtons() {
        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton()
                .setText(BACKWARD + " Back to main menu")
                .setCallbackData("main");

        List<InlineKeyboardButton> keyboardButtonsRow1 = Collections.singletonList(inlineKeyboardButton1);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        Collections.addAll(rowList, keyboardButtonsRow1);

        return new InlineKeyboardMarkup().setKeyboard(rowList);
    }

    public static InlineKeyboardMarkup setMainMenuButtons() {

        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton()
                .setText("Config Generator " + FORWARD)
                .setCallbackData(CONFIG_GENERATOR_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton()
                .setText("Docker Commands " + FORWARD)
                .setCallbackData(DOCKER_COMMANDS_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton3 = new InlineKeyboardButton()
                .setText("Faucets " + FORWARD)
                .setCallbackData(FAUCETS_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton4 = new InlineKeyboardButton()
                .setText("Address List " + FORWARD)
                .setCallbackData(ADDRESS_LIST_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton5 = new InlineKeyboardButton()
                .setText("GitHub Repositories " + FORWARD)
                .setCallbackData(GITHUB_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton6 = new InlineKeyboardButton()
                .setText("Comparison Guides " + FORWARD)
                .setCallbackData(GUIDES_CALLBACK);

        List<InlineKeyboardButton> keyboardButtonsRow1 = Collections.singletonList(inlineKeyboardButton1);
        List<InlineKeyboardButton> keyboardButtonsRow2 = Collections.singletonList(inlineKeyboardButton2);
        List<InlineKeyboardButton> keyboardButtonsRow3 = Collections.singletonList(inlineKeyboardButton3);
        List<InlineKeyboardButton> keyboardButtonsRow4 = Collections.singletonList(inlineKeyboardButton4);
        List<InlineKeyboardButton> keyboardButtonsRow5 = Collections.singletonList(inlineKeyboardButton5);
        List<InlineKeyboardButton> keyboardButtonsRow6 = Collections.singletonList(inlineKeyboardButton6);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        Collections.addAll(rowList, keyboardButtonsRow1, keyboardButtonsRow2, keyboardButtonsRow3, keyboardButtonsRow4,
                keyboardButtonsRow5, keyboardButtonsRow6);

        return new InlineKeyboardMarkup().setKeyboard(rowList);
    }

    public static InlineKeyboardMarkup setReportsButtons() {
        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton()
                .setText("TESTNET - Ropsten - Random Beacon " + FORWARD)
                .setCallbackData(TESTNET_ROPSTEN_RANDOM_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton()
                .setText("TESTNET - ECDSA " + FORWARD)
                .setCallbackData(TESTNET_ECDSA_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton3 = new InlineKeyboardButton()
                .setText("MAINNET - Ropsten - Random Beacon " + FORWARD)
                .setCallbackData(MAINNET_ROPSTEN_RANDOM_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton4 = new InlineKeyboardButton()
                .setText("MAINNET - ECDSA " + FORWARD)
                .setCallbackData(MAINNET_ECDSA_CALLBACK);
        InlineKeyboardButton inlineKeyboardButton5 = new InlineKeyboardButton()
                .setText(BACKWARD + " Back to main menu")
                .setCallbackData(BACK_TO_MAIN_CALLBACK);

        List<InlineKeyboardButton> keyboardButtonsRow1 = Collections.singletonList(inlineKeyboardButton1);
        List<InlineKeyboardButton> keyboardButtonsRow2 = Collections.singletonList(inlineKeyboardButton2);
        List<InlineKeyboardButton> keyboardButtonsRow3 = Collections.singletonList(inlineKeyboardButton3);
        List<InlineKeyboardButton> keyboardButtonsRow4 = Collections.singletonList(inlineKeyboardButton4);
        List<InlineKeyboardButton> keyboardButtonsRow5 = Collections.singletonList(inlineKeyboardButton5);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        Collections.addAll(rowList, keyboardButtonsRow1, keyboardButtonsRow2, keyboardButtonsRow3, keyboardButtonsRow4, keyboardButtonsRow5);

        return new InlineKeyboardMarkup().setKeyboard(rowList);
    }
}

