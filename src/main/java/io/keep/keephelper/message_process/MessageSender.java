package io.keep.keephelper.message_process;

import io.keep.keephelper.service.BotService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
@RequiredArgsConstructor
@Service
public class MessageSender implements Runnable {
    private final BotService botService;

    @Override
    public void run() {
        log.info("[KEEP-HELPER-I004] Send message");
        try {
            while (true) {
                Object update = BotService.sendQueue.take();
                log.info("[KEEP-HELPER-I005] Get new message to send: {}", update.toString());
                send(update);
            }
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E007] Unknown error occurred while send message: {}", e.getMessage(), e);
        }
    }

    private void send(Object object) {
        try {
            if (object instanceof SendMessage) {
                SendMessage sendMessage = (SendMessage) object;
                botService.execute(sendMessage);
            }

            if (object instanceof EditMessageText) {
                EditMessageText editMessageText = (EditMessageText) object;
                botService.execute(editMessageText);
            }

            if (object instanceof SendDocument) {
                SendDocument sendDocument = (SendDocument) object;
                botService.execute(sendDocument);
            }
        } catch (TelegramApiException e) {
            log.error("[KEEP-HELPER-E008] Error on execute: {}", e.getMessage(), e);
        }
    }
}
