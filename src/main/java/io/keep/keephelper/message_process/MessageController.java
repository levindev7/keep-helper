package io.keep.keephelper.message_process;

import io.keep.keephelper.controller.MessageFactory;
import io.keep.keephelper.service.BotService;
import io.keep.keephelper.service.interfaces.handlers.Command;
import io.keep.keephelper.utils.MessageHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.concurrent.ConcurrentHashMap;

import static io.keep.keephelper.utils.Constants.START_MESSAGE;
import static io.keep.keephelper.utils.Constants.TEMPLATE_TYPE;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageController {
    private final MessageFactory factory;

    public void processMessageController(Message message) {
        long chatId = message.getChatId();
        String msg = message.getText();
        try {
            BotService.sendQueue.put(commandHandler(chatId, msg));

        } catch (Exception e) {
            log.error("[KEEP-HELPER-E002] Unknown exception while handling command: {}", e.getMessage(), e);
        }
    }

    private SendMessage commandHandler(long chatId, String text) {
        if (text.startsWith("/")) {
            if (text.equals("/help")) {
                Command textCommand = factory
                        .getCommandOperation(text)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid command: " + text));

                return textCommand.getCommand(chatId);
            } else {
                return sendDefaultMessage(chatId, BotService.users);
            }
        } else {
            return sendDefaultMessage(chatId, BotService.users);
        }
    }

    private SendMessage sendDefaultMessage(long chatId, ConcurrentHashMap<Long, String> users) {
        String describe = TEMPLATE_TYPE;
        if (users.containsKey(chatId)) {
            users.replace(chatId, describe);
        } else {
            users.put(chatId, describe);
        }
        try {
            String text = START_MESSAGE;
            return new SendMessage()
                    .setChatId(chatId)
                    .enableMarkdown(true)
                    .setText(text)
                    .setReplyMarkup(MessageHelper.setMainMenuButtons());
        } catch (Exception e) {
            log.error("[KEEP-HELPER-E003] Error on execute bot command: {}, {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }
}
