package io.keep.keephelper.message_process;

import io.keep.keephelper.controller.MessageFactory;
import io.keep.keephelper.service.BotService;
import io.keep.keephelper.service.interfaces.handlers.Callback;
import io.keep.keephelper.utils.MessageHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import static io.keep.keephelper.utils.Constants.HELP_MESSAGE;
import static io.keep.keephelper.utils.Constants.START_MESSAGE;

@Slf4j
@Service
@RequiredArgsConstructor
public class CallbackController {
    private final MessageFactory factory;

    public void processCallbackController(CallbackQuery callbackQuery) {
        long callbackChatId = callbackQuery.getMessage().getChatId();
        String callbackData = callbackQuery.getData();
        int messageId = callbackQuery.getMessage().getMessageId();

        try {
                BotService.sendQueue.add(callbackHandler(callbackChatId, messageId, callbackData));

        } catch (Exception e) {
            log.error("[KEEP-HELPER-E001] Unknown exception while handling callback: {}", e.getMessage(), e);
        }
    }

    private EditMessageText callbackHandler(long chatId, int messageId, String callbackData) {
        if (BotService.users.get(chatId) == null) {
            return MessageHelper.editMessage(chatId, messageId, START_MESSAGE);
        }
        if (callbackData.equals("main")) {
            return MessageHelper.editMessageWithKeyboard(chatId, messageId, START_MESSAGE, MessageHelper.setMainMenuButtons());
        }
        Callback callbackMessage = factory
                .getCallbackOperation(callbackData)
                .orElseThrow(() -> new IllegalArgumentException("Invalid callback: " + callbackData));

        return callbackMessage.getCallback(chatId, messageId);
    }
}
