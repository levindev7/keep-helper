package io.keep.keephelper.message_process;

import io.keep.keephelper.service.BotService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Service
public class MessageReceiver implements Runnable {
    private final MessageController messageController;
    private final CallbackController callbackController;

    @Override
    public void run() {
        log.info("[KEEP-HELPER-I001] Receive message");
        while (true) {
            try {
                Update update = BotService.receiveQueue.take();
                log.info("[KEEP-HELPER-I002] New update for analyze in queue " + update.toString());
                if (isMessage(update)) {
                    messageController.processMessageController(update.getMessage());
                } else if (isCorrectCallback(update)) {
                    callbackController.processCallbackController(update.getCallbackQuery());
                }
            } catch (InterruptedException e) {
                log.error("[KEEP-HELPER-E007] Catch interrupt. Exit. {}", e.getMessage(), e);
                return;
            }
        }
    }

    private boolean isMessage(Update update) {
        return update.hasMessage() && update.getMessage().hasText();
    }

    private boolean isCorrectCallback(Update update) {
        return Objects.nonNull(update.getCallbackQuery().getMessage()) &&
                Objects.nonNull(update.getCallbackQuery().getMessage().getChatId()) &&
                Objects.nonNull(update.getCallbackQuery().getMessage().getMessageId()) &&
                Objects.nonNull(update.getCallbackQuery().getData());
    }
}
