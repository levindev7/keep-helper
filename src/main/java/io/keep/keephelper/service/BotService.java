package io.keep.keephelper.service;

import io.keep.keephelper.config.property.BotProperties;
import io.keep.keephelper.message_process.CallbackController;
import io.keep.keephelper.message_process.MessageController;
import io.keep.keephelper.message_process.MessageReceiver;
import io.keep.keephelper.message_process.MessageSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.generics.BotSession;
import org.telegram.telegrambots.starter.AfterBotRegistration;

import javax.annotation.PostConstruct;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
@RequiredArgsConstructor
@Service
public class BotService extends TelegramLongPollingBot {
    public static ConcurrentHashMap<Long, String> users = new ConcurrentHashMap<>();
    public static final BlockingQueue<Object> sendQueue = new LinkedBlockingQueue<>();
    public static final BlockingQueue<Update> receiveQueue = new LinkedBlockingQueue<>();

    private final BotProperties botProperties;
    private final MessageController messageController;
    private final CallbackController callbackController;

    public void onUpdateReceived(Update update) {
        log.info("[KEEP-HELPER-I006] Receive new Update. updateID: {}", update.getUpdateId());
        try {
            receiveQueue.put(update);
        } catch (InterruptedException e) {
            log.error("[KEEP-HELPER-E009] - Interrupt error while adding telegram update to queue");
        }
    }

    @Override
    public String getBotUsername() {
        return botProperties.getName();
    }

    @Override
    public String getBotToken() {
        return botProperties.getApiKey();
    }

    @PostConstruct
    private void ThreadStart() {
        MessageReceiver messageReceiver = new MessageReceiver(messageController, callbackController);
        MessageSender messageSender = new MessageSender(this);

        Thread receiver = new Thread(messageReceiver);
        receiver.setDaemon(true);
        receiver.setName("MessageReceiver");
        receiver.setPriority(3);
        receiver.start();

        Thread sender = new Thread(messageSender);
        sender.setDaemon(true);
        sender.setName("MessageSender");
        sender.setPriority(1);
        sender.start();
    }
}
