package io.keep.keephelper.service.interfaces.handlers;

import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

public interface Callback {
    /**
     Callback в телеграмм-боте - это событие, поступающее при нажатии на кнопку клавиатуры. Поступает в составе
     класса CallbackQuery.
     */
    EditMessageText getCallback(long chatId, int messageId);
}
