package io.keep.keephelper.service.interfaces.parsers;

public interface GuidesParser {
    /**
     * @param config - input info
     * @return String - edit info
     */
    String parse(String config);
}
