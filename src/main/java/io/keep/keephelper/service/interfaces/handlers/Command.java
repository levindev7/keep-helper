package io.keep.keephelper.service.interfaces.handlers;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public interface Command {
    /**
     Команды в телеграмм-боте начинаются с символа "/", доступны в панели вызова комманд,
     поступают на обработку в составе класса Message.
     */
    SendMessage getCommand(long chatId);
}
