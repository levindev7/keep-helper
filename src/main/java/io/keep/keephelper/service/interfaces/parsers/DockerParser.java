package io.keep.keephelper.service.interfaces.parsers;

public interface DockerParser {
    /**
     * @param config - input info
     * @return String - edit info
     */
    String parse(String config);
}
