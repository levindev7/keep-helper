package io.keep.keephelper.service.impl.parsers;


import io.keep.keephelper.service.interfaces.parsers.ConfigParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConfigParserImpl implements ConfigParser {

    public String parse(String config) {
        return config;
    }
}