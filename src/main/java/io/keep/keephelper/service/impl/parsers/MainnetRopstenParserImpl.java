package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.MainnetRopstenParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MainnetRopstenParserImpl implements MainnetRopstenParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder
                .append("\\[ethereum]\n" +
                        "URL = \"wss://mainnet.infura.io/ws/v3/\\[enter your Infura Project ID here]\"\n" +
                        "URLRPC = \"https://mainnet.infura.io/v3/\\[enter your Infura Project ID here]\"\n" +
                        "\n")
                .append("\n" +
                        "\\[ethereum.account]\n" +
                        "Address = \"Your Operator Address\"\n" +
                        "KeyFile = \"/mnt/keep-beacon-client/keystore/\"Your Beacon Keyfile Name\".json\"\n" +
                        "\n")
                .append("\\[ethereum.ContractAddresses]\n +" +
                        "KeepRandomBeaconOperator = \"0xdf708431162ba247ddae362d2c919e0fbafcf9de\"\n" +
                        "TokenStaking = \"0x1293a54e160d1cd7075487898d65266081a15458\"\n" +
                        "KeepRandomBeaconService = \"0x50510e691c90ea098e3fdd23c311731bf394aafd\"\n" +
                        "\n")
                .append("\\[LibP2P]\n" +
                        "Peers = \\[\"/ip4/54.39.179.73/tcp/3919/ipfs/16Uiu2HAkyYtzNoWuF3ULaA7RMfVAxvfQQ9YRvRT3TK4tXmuZtaWi\"\n" +
                        ",\"/ip4/54.39.186.166/tcp/3919/ipfs/16Uiu2HAkzD5n4mtTSddzqVY3wPJZmtvWjARTSpr4JbDX9n9PDJRh\"\n" +
                        ",\"/ip4/54.39.179.134/tcp/3919/ipfs/16Uiu2HAkuxCuWA4zXnsj9R6A3b3a1TKUjQvBpAEaJ98KGdGue67p\"\n" +
                        ",\"/dns4/bst-a01.core.keep.boar.network/tcp/3001/ipfs/16Uiu2HAkzYFHsqbwt64ZztWWK1hyeLntRNqWMYFiZjaKu1PZgikN\"\n" +
                        ",\"/dns4/bst-b01.core.keep.boar.network/tcp/3001/ipfs/16Uiu2HAkxLttmh3G8LYzAy1V1g1b3kdukzYskjpvv5DihY4wvx7D\"\n" +
                        ",\"/dns4/4d00662f-e56d-404a-803a-cac01ada3e15.keep.bison.run/tcp/3919/ipfs/16Uiu2HAmV3HqJjcbKMxHnDxDx4m2iEYynyYdsvU3VwaeE6Zra2P9\"\n" +
                        ",\"/dns4/ec1eb390-124c-4b1b-bcf7-c21709baf2b2.keep.herd.run/tcp/3919/ipfs/16Uiu2HAmVo51PqEZLADehZEbZnrp5A7qjRWFLj9E7DfwZKVhERFt\"\n" +
                        ",\"/dns4/2aa9b786-7360-4c22-ae73-bd95af9c11c5.keep.bison.run/tcp/3919/ipfs/16Uiu2HAm9g3QrQzSvJ8FAhgB1PmjMNgjPd3pDaJJqsdSisGsnaFe\"]\n" +
                        "  Port = 3919\n" +
                        "AnnouncedAddresses = [\"/ip4/[enter your Node Public IP here]\"/tcp/3920\"]\n" +
                        "\n")
                .append("\\[Storage]\n" +
                        "DataDir = \"/mnt/keep-beacon-client/persistence\"\n")
        ;

        return builder.toString();
    }
}
