package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.KeystoreParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KeystoreParserImpl implements KeystoreParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder.append("This functionality is currently being tested and will be available later.");
        return builder.toString();
    }
}
