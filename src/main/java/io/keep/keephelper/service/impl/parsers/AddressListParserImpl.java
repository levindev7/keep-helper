package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.AddressListParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AddressListParserImpl implements AddressListParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder.append("An up-to-date list of all of the relevant Ethereum addresses associated with Keep\n" +
                "\n" +
                "MAINNET - TOKENS\n" +
                "\n")
        .append("KEEP   -   https://etherscan.io/address/0x85eee30c52b0b379b046fb0f85f4f3dc3009afec\n" +
                "tBTC   -   https://etherscan.io/address/0x8dAEBADE922dF735c38C80C7eBD708Af50815fAa\n" +
                "tBTC Deposit Token (TDT)   -\n" +
                "https://etherscan.io/address/0x10B66Bd1e3b5a936B7f8Dbc5976004311037Cdf0\n" +
                "\n" +
                "")
        .append("MAINNET - RANDOM BEACON NODE\n" +
                "\n")
        .append("TokenStaking   -   https://etherscan.io/address/0x1293a54e160d1cd7075487898d65266081a15458\n" +
                "KeepRandomBeaconService   -   https://etherscan.io/address/0x50510e691c90ea098e3fdd23c311731bf394aafd\n" +
                "KeepRandomBeaconOperator   -   https://etherscan.io/address/0xdf708431162ba247ddae362d2c919e0fbafcf9de\n" +
                "\n")
        .append("MAINNET - ECDSA NODE\n" +
                "\n")
        .append("BondedECDSAKeepFactory   -   https://etherscan.io/address/0xA7d9E842EFB252389d613dA88EDa3731512e40bD\n" +
                "Sanctioned Applications   -   https://etherscan.io/address/0xe20A5C79b39bC8C363f0f49ADcFa82C2a01ab64a\n" +
                "\n")
        .append("ROPSTEN - TOKENS\n" +
                "\n")
        .append("KEEP   -   https://ropsten.etherscan.io/address/0x343d3DDA00415289CDD4E8030F63a4A5a2548ff9\n" +
                "tBTC   -   https://ropsten.etherscan.io/address/0x7c07C42973047223F80C4A69Bb62D5195460Eb5F\n" +
                "tBTC Deposit Token (TDT)   -   \n" +
                "https://ropsten.etherscan.io/address/0x7cAad48DF199Cd661762485fc44126F4Fe8A58C9\n" +
                "\n")
        .append("ROPSTEN - RANDOM BEACON NODE\n" +
                "\n")
        .append("TokenStaking   -   https://ropsten.etherscan.io/address/0x234d2182B29c6a64ce3ab6940037b5C8FdAB608e\n" +
                "KeepRandomBeaconService   -   https://ropsten.etherscan.io/address/0x6c04499B595efdc28CdbEd3f9ed2E83d7dCCC717\n" +
                "KeepRandomBeaconOperator   -   https://ropsten.etherscan.io/address/0xC8337a94a50d16191513dEF4D1e61A6886BF410f\n" +
                "\n")
        .append("ROPSTEN - ECDSA NODE\n" +
                "\n")
        .append("BondedECDSAKeepFactory   -   https://ropsten.etherscan.io/address/0x9EcCf03dFBDa6A5E50d7aBA14e0c60c2F6c575E6\n" +
                "Sanctioned Applications   -   https://ropsten.etherscan.io/address/0xc3f96306eDabACEa249D2D22Ec65697f38c6Da69")
        ;
        return builder.toString();
    }
}
