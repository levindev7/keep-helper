package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.DockerParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class DockerParserImpl implements DockerParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder.append("Cheatsheet for commands to create, start, restart, and inspect your Keep nodes\n" +
                "\n" +
                "PARAMETERS\n" +
                "\n" +
                "Beacon Config Path: ~/keep/beacon/config\n" +
                "Beacon Persistence Path: ~/keep/beacon/persistence\n" +
                "Beacon Keystore Path: ~/keep/beacon/keystore\n" +
                "Beacon Key Password:\n" +
                "ECDSA Config Path: ~/keep/ecdsa/config\n" +
                "ECDSA Persistence Path: ~/keep/ecdsa/persistence\n" +
                "ECDSA Keystore Path: ~/keep/ecdsa/keystore\n" +
                "ECDSA Key Password \n" +
                "\n" +
                "SETUP AND START - RANDOM BEACON\n")
        .append("docker run -d \\\n" +
                "--entrypoint keep-client \\\n" +
                "--restart always \\\n" +
                "--volume ~/keep/beacon/config:/mnt/keep-beacon-client/config \\\n" +
                "--volume ~/keep/beacon/persistence:/mnt/keep-beacon-client/persistence \\\n" +
                "--volume ~/keep/beacon/keystore:/mnt/keep-beacon-client/keystore \\\n" +
                "--env KEEP_ETHEREUM_PASSWORD=\"\" \\\n" +
                "--env LOG_LEVEL=debug \\\n" +
                "--log-opt max-size=100m \\\n" +
                "--log-opt max-file=3 \\\n" +
                "--name beacon-node \\\n" +
                "-p 3920:3919 \\\n" +
                "keepnetwork/keep-client:v1.3.0 --config /mnt/keep-beacon-client/config/config.toml start\n")
        .append("\n" +
                "SETUP AND START - ECDSA\n" +
                "\n")
        .append("docker run -d \\\n" +
                "--entrypoint keep-ecdsa \\\n" +
                "--restart always \\\n" +
                "--volume ~/keep/ecdsa/config:/mnt/keep-ecdsa-client/config \\\n" +
                "--volume ~/keep/ecdsa/persistence:/mnt/keep-ecdsa-client/persistence \\\n" +
                "--volume ~/keep/ecdsa/keystore:/mnt/keep-ecdsa-client/keystore \\\n" +
                "--env KEEP_ETHEREUM_PASSWORD=\"\" \\\n" +
                "--env LOG_LEVEL=debug \\\n" +
                "--log-opt max-size=100m \\\n" +
                "--log-opt max-file=3 \\\n" +
                "--name ecdsa-node \\\n" +
                "-p 3919:3919 \\\n" +
                "keepnetwork/keep-ecdsa-client:v1.2.1 --config /mnt/keep-ecdsa-client/config/config.toml start\n")
        .append("\n" +
                "LIST CONTAINERS\n" +
                "Use this to check the status of your nodes\n" +
                "\n" +
                "docker ps\n" +
                "")
        .append("\n" +
                "LOGS\n" +
                "Add parameters like --since 30m for logs from past 30 minutes or -f to continuously follow logs\n" +
                "\n" +
                "docker logs beacon-node\n" +
                "docker logs ecdsa-node\n")
        .append("\n" +
                "RESTART\n" +
                "\n" +
                "docker restart beacon-node\n" +
                "docker restart ecdsa-node\n")
        .append("\n" +
                "START\n" +
                "\n" +
                "docker start beacon-node\n" +
                "docker start ecdsa-node\n")
        .append("\n" +
                "STOP\n" +
                "\n" +
                "docker stop beacon-node\n" +
                "docker stop ecdsa-node\n")
        .append("\n" +
                "DELETE\n" +
                "\n" +
                "docker rm beacon-node\n" +
                "docker rm ecdsa-node\n")
        .append("\n" +
                "DOCKER HELP\n" +
                "\n" +
                "docker help\n")
        ;
        return builder.toString();
    }
}
