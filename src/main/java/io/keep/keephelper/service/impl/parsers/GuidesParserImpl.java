package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.GuidesParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class GuidesParserImpl implements GuidesParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder.append("VALIDATOR GUIDES\n" +
                "Note: This page is currently a work-in-progress.\n" +
                "\n" +
                "");
        return builder.toString();
    }
}
