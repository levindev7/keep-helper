package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.TestnetECDSAParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestnetECDSAParserImpl implements TestnetECDSAParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder
                .append("\\[ethereum]\n" +
                        "URL = \"wss://ropsten.infura.io/ws/v3/\\[enter your Infura Project ID here]\"\n" +
                        "URLRPC = \"https://ropsten.infura.io/v3/\\[enter your Infura Project ID here]\"\n" +
                        "\n")
                .append("\n" +
                        "\\[ethereum.account]\n" +
                        "Address = \"Your Operator Address\"\n" +
                        "KeyFile = \"/mnt/keep-ecdsa-client/keystore/\"Your ECDSA Keyfile Name\".json\"\n" +
                        "\n")
                .append("\\[ethereum.ContractAddresses]\n +" +
                        "BondedECDSAKeepFactory = \"0x9EcCf03dFBDa6A5E50d7aBA14e0c60c2F6c575E6\"\n" +
                        "\n")
                .append("\\[SanctionedApplications]\n" +
                        "Addresses = \\[\n" +
                        "    \"0xc3f96306eDabACEa249D2D22Ec65697f38c6Da69\"\n" +
                        "  ]\n")
                .append("\\[LibP2P]\n" +
                        "Peers = \\[\"/dns4/bootstrap-\n" +
                        "1.ecdsa.keep.test.boar.network/tcp/4001/ipfs/16Uiu2HAmPFXDaeGWtnzd8s39NsaQguoWtKi77834A6xwYqeicq6N\",\"/dns4/ecdsa-\n" +
                        "2.test.keep.network/tcp/3919/ipfs/16Uiu2HAmNNuCp45z5bgB8KiTHv1vHTNAVbBgxxtTFGAndageo9Dp\",\"/dns4/ecdsa-\n" +
                        "3.test.keep.network/tcp/3919/ipfs/16Uiu2HAm8KJX32kr3eYUhDuzwTucSfAfspnjnXNf9veVhB12t6Vf\"]\n" +
                        "  Port = 3919\n" +
                        "\n")
                .append("\\[Storage]\n" +
                        "DataDir = \"/mnt/keep-ecdsa-client/persistence\"\n" +
                        "\n")
                .append("\\[TSS]\n" +
                        "PreParamsGenerationTimeout = \"2m30s\"")
        ;

        return builder.toString();
    }
}
