package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.TestnetRopstenParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestnetRopstenParserImpl implements TestnetRopstenParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder
                .append("\\[ethereum]\n" +
                        "URL = \"wss://ropsten.infura.io/ws/v3/\\[enter your Infura Project ID here]\"\n" +
                        "URLRPC = \"https://ropsten.infura.io/v3/\\[enter your Infura Project ID here]\"\n" +
                        "\n")
                .append("\n" +
                        "\\[ethereum.account]\n" +
                        "Address = \"Your Operator Address\"\n" +
                        "KeyFile = \"/mnt/keep-beacon-client/keystore/\"Your Beacon Keyfile Name\".json\"\n" +
                        "\n")
                .append("\\[ethereum.ContractAddresses]\n +" +
                        "KeepRandomBeaconOperator = \"0xC8337a94a50d16191513dEF4D1e61A6886BF410f\"\n" +
                        "TokenStaking = \"0x234d2182B29c6a64ce3ab6940037b5C8FdAB608e\"\n" +
                        "KeepRandomBeaconService = \"0x6c04499B595efdc28CdbEd3f9ed2E83d7dCCC717\"\n" +
                        "\n")
                .append("\\[LibP2P]\n" +
                        "Peers = \\[\"/dns4/bootstrap-\n" +
                        "1.core.keep.test.boar.network/tcp/3001/ipfs/16Uiu2HAkuTUKNh6HkfvWBEkftZbqZHPHi3Kak5ZUygAxvsdQ2UgG\",\"/dns4/bootstrap-\n" +
                        "2.core.keep.test.boar.network/tcp/3001/ipfs/16Uiu2HAmQirGruZBvtbLHr5SDebsYGcq6Djw7ijF3gnkqsdQs3wK\",\"/dns4/bootstrap-\n" +
                        "3.test.keep.network/tcp/3919/ipfs/16Uiu2HAm8KJX32kr3eYUhDuzwTucSfAfspnjnXNf9veVhB12t6Vf\",\"/dns4/bootstrap-2.test.keep.network/tcp/3919/ipfs/16Uiu2HAmNNuCp45z5bgB8KiTHv1vHTNAVbBgxxtTFGAndageo9Dp\"]\n" +
                        "Port = 3919\n" +
                        "AnnouncedAddresses = [\"/ip4/[enter your Node Public IP here]\"/tcp/3920\"]\n" +
                        "\n")
                .append("\\[Storage]\n" +
                        "DataDir = \"/mnt/keep-beacon-client/persistence\"\n")
        ;

        return builder.toString();
    }
}
