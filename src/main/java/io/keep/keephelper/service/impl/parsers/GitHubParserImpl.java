package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.GitHubParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class GitHubParserImpl implements GitHubParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder.append("A convenient index of GitHub repositories associated with the Keep project\n" +
                "\n" +
                "COMMON REPOSITORIES\n" +
                "\n")
        .append("Keep Ethereum smart contracts and client for Random Beacon nodes -\n" +
                "https://github.com/keep-network/keep-core/\n" +
                "\n" +
                "Keep Ethereum smart contracts and client for ECDSA nodes -\n" +
                "https://github.com/keep-network/keep-ecdsa/\n" +
                "\n" +
                "tBTC Ethereum smart contracts and specification -\n" +
                "https://github.com/keep-network/tbtc/\n" +
                "\n" +
                "JavaScript bindings for tBTC -\n" +
                "https://github.com/keep-network/tbtc.js/\n" +
                "\n" +
                "Dapp for depositing BTC and redeeming tBTC -\n" +
                "https://github.com/keep-network/tbtc-dapp/\n" +
                "\n" +
                "Whitepaper describing the Keep Network -\n" +
                "https://github.com/keep-network/whitepaper/\n" +
                "\n")
        .append("ADDITIONAL REPOSITORIES\n" +
                "\n" +
                "Scripts for local development and testing of Keep and tBTC -\n" +
                "https://github.com/keep-network/local-setup/\n" +
                "\n" +
                "Common libraries and tools used across Keep -\n" +
                "https://github.com/keep-network/keep-common/\n" +
                "\n" +
                "Keep Network website -\n" +
                "https://github.com/keep-network/website/\n" +
                "\n" +
                "tBTC website -\n" +
                "https://github.com/keep-network/tbtc-website/\n" +
                "\n" +
                "Yellowpaper describing the Keep Random Beacon -\n" +
                "https://github.com/keep-network/random-beacon-yellowpaper/")
        ;
        return builder.toString();
    }
}
