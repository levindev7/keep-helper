package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.MainnetECDSAParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MainnetECDSAParserImpl implements MainnetECDSAParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder
                .append("\\[ethereum]\n" +
                        "URL = \"wss://mainnet.infura.io/ws/v3/\\[enter your Infura Project ID here]\"\n" +
                        "URLRPC = \"https://mainnet.infura.io/v3/\\[enter your Infura Project ID here]\"\n" +
                        "\n")
                .append("\n" +
                        "\\[ethereum.account]\n" +
                        "Address = \"Your Operator Address\"\n" +
                        "KeyFile = \"/mnt/keep-ecdsa-client/keystore/\"Your ECDSA Keyfile Name\".json\"\n" +
                        "\n")
                .append("\\[ethereum.ContractAddresses]\n +" +
                        "BondedECDSAKeepFactory = \"0xA7d9E842EFB252389d613dA88EDa3731512e40bD\"\n" +
                        "\n")
                .append("\\[SanctionedApplications]\n" +
                        "Addresses = \\[\n" +
                        "    \"0xe20A5C79b39bC8C363f0f49ADcFa82C2a01ab64a\"\n" +
                        "  ]\n")
                .append("\\[LibP2P]\n" +
                        "Peers = \\[\"/dns4/bst-a01.ecdsa.keep.boar.network/tcp/4001/ipfs/16Uiu2HAkzYFHsqbwt64ZztWWK1hyeLntRNqWMYFiZjaKu1PZgikN\"\n" +
                        ",\"/dns4/bst-b01.ecdsa.keep.boar.network/tcp/4001/ipfs/16Uiu2HAkxLttmh3G8LYzAy1V1g1b3kdukzYskjpvv5DihY4wvx7D\"]\n" +
                        "  Port = 3919\n" +
                        "\n")
                .append("\\[Storage]\n" +
                        "DataDir = \"/mnt/keep-ecdsa-client/persistence\"\n" +
                        "\n")
                .append("\\[TSS]\n" +
                        "PreParamsGenerationTimeout = \"2m30s\"")
        ;

        return builder.toString();
    }
}
