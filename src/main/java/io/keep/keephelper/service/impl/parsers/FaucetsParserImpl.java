package io.keep.keephelper.service.impl.parsers;

import io.keep.keephelper.service.interfaces.parsers.FaucetsParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class FaucetsParserImpl implements FaucetsParser {
    @Override
    public String parse(String config) {
        StringBuilder builder = new StringBuilder();
        builder.append("Get Ropsten ETH, Testnet BTC, and a KEEP token grant for testing purposes\n" +
                "\n" +
                "ROPSTEN KEEP\n" +
                "\n" +
                "Operator address (add your eth address after =): \n" +
                "https://us-central1-keep-test-f3e0.cloudfunctions.net/keep-faucet-ropsten?account= \n" +
                "\n" +
                "\n" +
                "ROPSTEN ETH\n" +
                "\n" +
                "https://faucet.metamask.io   -   1 ETH x 5 times per hour, gets cranky if balance > 5 ETH\n" +
                "https://bitaps.com   -   1 ETH per minute\n" +
                "https://faucet.ropsten.be   -   Bans you for requesting frequently\n" +
                "")
        .append("\n" +
                "\n" +
                "TESTNET BTC\n" +
                "\n" +
                "https://bitcointalk.org   -   List of Testnet BTC faucets with comments")
        ;
        return builder.toString();
    }
}
