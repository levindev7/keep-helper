package io.keep.keephelper.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigBeacon {
    private String InfuraProjectID;
    private String OperatorAddress;
    private String BeaconKeyfileName;
    private String NodePublicIP;
}
